
FROM node:15-alpine as build-stage
RUN apk add --no-cache python3 py3-pip
WORKDIR /app
COPY package*.json ./
RUN npm install
COPY . /app
CMD node app.js

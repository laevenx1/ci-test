module.exports = {
    "collectCoverage": true,
    "collectCoverageFrom": ["**/*.js", "!**/node_modules/**"],
    "coverageReporters": ["html", "text", "text-summary", "cobertura"],
    "testMatch": ["**/*.test.js"]
  }
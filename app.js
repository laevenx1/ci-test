const express = require('express')
const app = express()
const morgan = require('morgan')
const cors = require('cors')

app.use(morgan('combined'))
app.use(cors())
app.use(express.urlencoded({extended : true}))
app.use(express.json())

app.use('/',(req,res) => {
    res.status(200).json({
        message: 'updated new kube config, in staging'
    })
})



module.exports = app